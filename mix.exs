defmodule Nrf24.MixProject do
  use Mix.Project

  @version "1.0.0"

  def project do
    [
      app: :nrf24,
      version: @version,
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: docs(),
      description: description(),
      package: package(),
      source_url: "https://gitlab.com/boskoivanisevic/nrf24"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:circuits_spi, "~> 1.3"},
      {:circuits_gpio, "~> 1.0"},
      {:ex_doc, "~> 0.27", only: :dev, runtime: false}
    ]
  end

  defp docs do
    [
      authors: ["Bosko Ivanišević"],
      assets: ["assets"],
      source_ref: "v#{@version}",
      extras: ["README.md"]
    ]
  end

  defp description do
    "Library for receiving/transmitting data over nRF24L01+ transciever."
  end

  defp package do
    [
      files: ~w(lib .formatter.exs mix.exs README* LICENSE* assets),
      licenses: ["Apache-2.0"],
      maintainers: ["Boško Ivanišević"],
      links: %{
        "GitLab" => "https://gitlab.com/boskoivanisevic/nrf24",
        "nRF24L01+ datasheet" =>
          "https://raw.githubusercontent.com/nRF24/RF24/master/datasheets/nRF24L01_datasheet_v2.pdf"
      }
    ]
  end
end
