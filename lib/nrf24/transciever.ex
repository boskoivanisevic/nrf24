defmodule Nrf24.Transciever do
  @moduledoc """
  Module for communicating with nRF24L01+ transciever.

  It sets up device, by default, to work in ShockBurst mode with ACK
  (auto acknowledgement) and CRC check enabled.
  """
  import Bitwise

  alias Circuits.SPI
  alias Circuits.GPIO

  @low 0
  @high 1

  @doc """
  Sets a channel on which tranciever will receive/send data.
  """
  def set_channel(spi, channel) when 1 <= channel and channel <= 255 do
    write_register(spi, :rf_ch, <<channel>>)
  end

  def set_channel(_, _) do
    {:error, :invalid_channel_value}
  end

  @doc """
  Returns a channel on which tranciever is working.
  """
  def get_channel(spi) do
    read_register(spi, :rf_ch)
  end

  @doc """
  Sets CRC length. Allowed values are 1 or 2.
  """
  def set_crc_length(spi, length) when 1 == length or 2 == length do
    config =
      case length do
        1 ->
          read_register(spi, :config) &&& reset_bit(:crco)

        2 ->
          read_register(spi, :config) ||| set_bit(:crco)
      end

    write_register(spi, :config, config)
  end

  def set_crc_length(_, _) do
    {:error, "Invalid CRC length"}
  end

  @doc """
  Configures transciever to receive data.
  """
  def set_receive_mode(spi) do
    # Set receive mode by setting PRIM_RX bit in config register
    config = read_register(spi, :config) ||| set_bit(:prim_rx)

    write_register(spi, :config, config)
  end

  @doc """
  Configures transciever to send data.
  """
  def set_transmit_mode(spi) do
    # Set transmit mode by resetting PRIM_RX bit in config register
    config = read_register(spi, :config) &&& reset_bit(:prim_rx)

    write_register(spi, :config, config)
  end

  def power_on(spi) do
    config = read_register(spi, :config) ||| set_bit(:pwr_up)

    write_register(spi, :config, config)
  end

  def power_off(spi) do
    config = read_register(spi, :config) &&& reset_bit(:pwr_up)

    write_register(spi, :config, config)
  end

  @doc """
  Sets data speed rate.

  * :low - speed 250Kbps
  * :medium - speed 1Mbps
  * :high - speed 2Mbps
  """
  def set_speed(spi, speed) when :min == speed or :medium == speed or :max == speed do
    rf_setup =
      case speed do
        :low ->
          (read_register(spi, :rf_setup) ||| set_bit(:rf_dr_low)) &&& reset_bit(:rf_dr_high)

        :medium ->
          read_register(spi, :rf_setup) &&& reset_bit(:rf_dr_low) &&& reset_bit(:rf_dr_high)

        :high ->
          (read_register(spi, :rf_setup) &&& reset_bit(:rf_dr_low)) ||| set_bit(:rf_dr_high)
      end

    write_register(spi, :rf_setup, rf_setup)
  end

  def set_speed(_, _) do
    {:error, :invalid_speed_value}
  end

  @doc """
  Sets 5-byte address for pipes 0 and 1 (values between 0 and 255) and
  single byte address for pipes 2-5.

  For pipes 0 and 1 it is good practice to set mnemonic pipe address

  set_pipe_address(spi, 0, "Recv1")

  but address can also be set with 5-byte binary

  set_pipe_address(spi, 1, <<0xe7, 0xa0, 0x17, 0x13, 0x25>>)

  For pipes 2-5 address is just integer between 0 and 255

  set_pipe_address(spi, 4, 147)
  """
  def set_pipe_address(spi, pipe_no, address = <<_::8, _::8, _::8, _::8, _::8>>)
      when (is_binary(address) and 0 == pipe_no) or 1 == pipe_no do
    reg_name = "rx_addr_p#{pipe_no}" |> String.to_atom()

    write_register(spi, reg_name, address)
  end

  def set_pipe_address(spi, pipe_no, address)
      when is_integer(address) and 2 <= pipe_no and pipe_no <= 5 do
    reg_name = "rx_addr_p#{pipe_no}" |> String.to_atom()

    write_register(spi, reg_name, <<address>>)
  end

  def set_pipe_address(_, _, _) do
    {:error, :invalid_pipe_address}
  end

  @doc """
  Sets address on which transciever will transmit data. Trasmit
  address is 5-bytes long and if auto-acknowledgement is turned on it
  has to be the same as address of the pipe 0.
  """
  def set_transmit_address(spi, address = <<_::8, _::8, _::8, _::8, _::8>>) do
    write_register(spi, :tx_addr, address)
  end

  def set_transmit_address(_, _) do
    {:error, :invalid_transmit_address}
  end

  @doc """
  Sets the size of data to be received on the given pipe.
  """
  def set_payload_size(spi, pipe_no, payload_size)
      when 1 <= payload_size and payload_size <= 32 do
    reg_name = "rx_pw_p#{pipe_no}" |> String.to_atom()

    write_register(spi, reg_name, payload_size)
  end

  def set_payload_size(_, _, _) do
    {:error, :invalid_ayload_size}
  end

  @doc """
  Turns auto-acknowledgement on and off.
  """
  def ack(spi, pipe_no, on \\ true) when 0 <= pipe_no and pipe_no <= 5 do
    ack_pipe_bit = "enaa_p#{pipe_no}"

    if on do
      # Set pipe's corresponding bit in en_aa register to 1
      write_register(spi, :en_aa, read_register(spi, :en_aa) ||| set_bit(ack_pipe_bit))
    else
      # Set pipe's corresponding bit in en_aa register to 0
      write_register(spi, :en_aa, read_register(spi, :en_aa) &&& reset_bit(ack_pipe_bit))
    end
  end

  def enable_pipe(spi, pipe_no) do
    rx_pipe_bit = "erx_p#{pipe_no}"

    write_register(spi, :en_rxaddr, read_register(spi, :en_rxaddr) ||| set_bit(rx_pipe_bit))
  end

  def disable_pipe(spi, pipe_no) do
    rx_pipe_bit = "erx_p#{pipe_no}"

    write_register(spi, :en_rxaddr, read_register(spi, :en_rxaddr) &&& reset_bit(rx_pipe_bit))
  end

  @doc """
  Set auto Retransmit Delay
  ‘0000’ – Wait 250μS
  ‘0001’ – Wait 500μS
  ‘0010’ – Wait 750μS
  ........
  ‘1111’ – Wait 4000μS
  (Delay defined from end of transmission to start of
  next transmission)

  Please take care when setting this parameter. If the ACK payload is
  more than 15 byte in 2Mbps mode the ARD must be 500μS or more, if the
  ACK payload is more than 5byte in 1Mbps mode the ARD must be 500μS or
  more. In 250kbps mode (even when the payload is not in ACK) the ARD
  must be 500μS or more.
  """
  def set_retransmit_delay(spi, delay) when 0 <= delay and delay <= 15 do
    write_register(spi, :setup_retr, delay <<< 4)
  end

  def set_retransmit_delay(_, _) do
    {:error, :invalid_retransmit_delay_value}
  end

  @doc """
  Sets a number of retransmits before trasmit is considered as failed.
  """
  def set_retransmit_count(spi, count) when 0 <= count and count <= 15 do
    write_register(spi, :setup_retr, count)
  end

  def set_retransmit_count(_, _) do
    {:error, :invalid_retransmit_count_value}
  end

  @doc """
  Resets RX and TX FIFO interrupts and maximum number of TX retrasmits.
  """
  def reset_status(spi) do
    status =
      read_register(spi, :status) |||
        set_bit(:rx_dr) |||
        set_bit(:tx_ds) |||
        set_bit(:max_rt)

    write_register(spi, :status, status)
  end

  @doc """
  Sets pipe's address, payload size and enables pipe in en_rxaddr
  register.

  For pipes 0 and 1 dddress must be 5 bytes length binary, i.e.

  <<0xe1, 0xc0, 0x00, 0x01, 0xa0>>

  For simpler, easier to remember case, address can be just a
  five-characters string, i.e.

  "Rcvr1"

  For pipes 2 to 5 address must be binary consisting of only one
  integer between 0 and 255.

  <<0xce>>

  Anything else including binary of different size will fallback to
  default pipe address.

  ## Options

    * `:address` - Pipe address. For first two pipes (0 and 1) it is
      expected to be 5 integers binary and for pipes 2 to 5 one
      integer binary. Anything else will just use default pipe
      addresses.

    * `:payload_size` - Pipe payload size in bytes.

    * `:auto_acknowledgement` - If false auto acknowledgement for the
      pipe will be disabled. If nil auto acknowledgement will be
      enabled by default.

  ## Example

      # Enable pipe1 with address 0x0a0b0c0d0e and 6 bytes
      # payload size
      {:ok, spi} = Circuits.SPI.open("spidev0.0")
      address = <<0x0a, 0x0b, 0x0c, 0x0d, 0x0e>>
      Nrf24.set_pipe(spi, 1, address: address, payload_size: 6)

      # Enable pipe 4 with addres 0xa7 and 12 bytes payload size
      Nrf24.set_pipe(spi, 4, address: <<0xa7>>, payload_size: 12)
  """
  def set_pipe(spi, pipe_no, options \\ []) do
    address = Keyword.get(options, :address)
    payload_size = Keyword.get(options, :payload_size, 32)
    auto_ack = Keyword.get(options, :auto_acknowledgement, true)

    with {:ok, _} <- set_pipe_address(spi, pipe_no, address),
         {:ok, _} <- set_payload_size(spi, pipe_no, payload_size),
         {:ok, _} <- ack(spi, pipe_no, auto_ack),
         {:ok, _} <- enable_pipe(spi, pipe_no) do
      {:ok, nil}
    else
      {:error, error} -> {:error, error}
    end
  end

  @doc """
  Configures transciever for receiving data, resets status and powers
  on transciever.

  This function must be called before data is received.
  """
  def start_listening(spi, ce_pin_no) do
    with {:ok, _} <- set_receive_mode(spi),
         {:ok, _} <- power_on(spi),
         {:ok, _} <- reset_status(spi),
         {:ok, ce} <- GPIO.open(ce_pin_no, :output) do
      GPIO.write(ce, @high)
      GPIO.close(ce)

      {:ok, nil}
    else
      {:error, error} -> {:error, error}
    end
  end

  @doc """
  Powers off transciever and sets CE pin to low.

  Call this function after data is received.
  """
  def stop_listening(spi, ce_pin_no) do
    with {:ok, _} <- power_off(spi),
         {:ok, ce} <- GPIO.open(ce_pin_no, :output) do
      GPIO.write(ce, @low)
      GPIO.close(ce)

      {:ok, nil}
    else
      {:error, error} -> {:error, error}
    end
  end

  @doc """
  Reads received data of `payload_size` size.

  If data is received on any pipe corresponding value in the status
  register will be set to pipe number and function will read data
  from FIFO register.

  Function returns map with pipe no on which data was received and
  received value.
  """
  def read_data(spi, payload_size) do
    fifo_empty = 7
    bits_size = payload_size * 8

    case <<read_register(spi, :status)>> do
      <<_::1, _rx_dr::1, _tx_ds::1, _max_rt::1, ^fifo_empty::3, _tx_full::1>> ->
        :no_data

      <<_::1, _rx_dr::1, _tx_ds::1, _max_rt::1, pipe_no::3, _tx_full::1>> ->
        full_cmd =
          for _ <- 1..payload_size, reduce: <<command(:r_rx_payload)>> do
            acc -> acc <> <<0x00>>
          end

        case SPI.transfer(spi, full_cmd) do
          {:ok, <<_dummy::integer, value::float-little-size(bits_size)>>} ->
            {:ok, %{pipe: pipe_no, value: value}}

          _ ->
            {:error, "Invalid data read from FIFO"}
        end
    end
  end

  def start_sending(spi, address) do
    with {:ok, _} <- set_transmit_mode(spi),
         {:ok, _} <- set_pipe_address(spi, 0, address),
         {:ok, _} <- set_transmit_address(spi, address),
         {:ok, _} <- ack(spi, 0, true),
         {:ok, _} <- reset_status(spi),
         {:ok, _} <- power_on(spi) do
      :ok
    else
      {:error, error} -> {:error, error}
    end
  end

  def stop_sending(ce) do
    Circuits.GPIO.write(ce, @low)
    Circuits.GPIO.close(ce)
  end

  @doc """
  Sends data of 1-32 bytes length. Always uses auto acknowledgment.
  """
  def send_data(spi, data, csn_pin, ce_pin)
      when is_binary(data) and byte_size(data) <= 32 do
    # In the manual it is stated that send command should be issues
    # keeping CSN pin low.
    {:ok, csn} = Circuits.GPIO.open(csn_pin, :output)
    Circuits.GPIO.write(csn, @low)

    cmd = <<command(:w_tx_payload)>> <> data

    Circuits.SPI.transfer(spi, cmd)

    Circuits.GPIO.close(csn)

    {:ok, ce} = Circuits.GPIO.open(ce_pin, :output)
    Circuits.GPIO.write(ce, @high)

    {:ok, ce}
  end

  @doc """
  Resets device to initial settings.
  """
  def reset(spi) do
    write_register(spi, :config, 0x0E)

    write_register(spi, :rx_addr_p0, <<0xE7, 0xE7, 0xE7, 0xE7, 0xE7>>)
    write_register(spi, :rx_addr_p1, <<0xC2, 0xC2, 0xC2, 0xC2, 0xC2>>)
    write_register(spi, :rx_addr_p2, 0xC3)
    write_register(spi, :rx_addr_p3, 0xC4)
    write_register(spi, :rx_addr_p4, 0xC5)
    write_register(spi, :rx_addr_p5, 0xC6)

    write_register(spi, :tx_addr, <<0xE7, 0xE7, 0xE7, 0xE7, 0xE7>>)

    write_register(spi, :rx_pw_p0, 0)
    write_register(spi, :rx_pw_p1, 0)
    write_register(spi, :rx_pw_p2, 0)
    write_register(spi, :rx_pw_p3, 0)
    write_register(spi, :rx_pw_p4, 0)
    write_register(spi, :rx_pw_p5, 0)

    write_register(spi, :config, 0x08)
    write_register(spi, :en_aa, 0x3F)
    write_register(spi, :en_rxaddr, 0x03)
    write_register(spi, :rf_ch, 0x02)
    write_register(spi, :rf_setup, 0x0F)
  end

  @doc """
  Function prints out status of registers. Useful mostly for debugging
  purposes.
  """
  def print_details(spi) do
    IO.puts(status_to_string(spi))
    IO.puts(addresses_to_string(spi))
    IO.puts(payload_size_as_string(spi))
    IO.puts(config_to_string(spi))
    IO.puts(register_to_string(:en_aa, spi))
    IO.puts(register_to_string(:en_rxaddr, spi))
    IO.puts(register_to_string(:rf_ch, spi))
    IO.puts(register_to_string(:rf_setup, spi))
    IO.puts(fifo_status_to_string(spi))
  end

  def write_register(spi, reg, value) when is_integer(value) do
    full_cmd = command(:w_register) ||| register(reg)

    SPI.transfer(spi, <<full_cmd, value>>)
  end

  def write_register(spi, reg, value) when is_binary(value) do
    full_cmd = command(:w_register) ||| register(reg)

    SPI.transfer(spi, <<full_cmd>> <> value)
  end

  def read_register(spi, reg, bytes_no \\ 1) do
    full_cmd =
      for _ <- 1..bytes_no, reduce: <<command(:r_register) ||| register(reg)>> do
        acc -> acc <> <<0xFF>>
      end

    if 1 == bytes_no do
      {:ok, <<_::size(8), res>>} = SPI.transfer(spi, full_cmd)

      res
    else
      {:ok, <<_::size(8), res::bitstring>>} = SPI.transfer(spi, full_cmd)

      res
    end
  end

  defp register(reg) do
    case reg do
      :config -> 0x00
      :en_aa -> 0x01
      :en_rxaddr -> 0x02
      :setup_aw -> 0x03
      :setup_retr -> 0x04
      :rf_ch -> 0x05
      :rf_setup -> 0x06
      :status -> 0x07
      :observe_tx -> 0x08
      :rpd -> 0x09
      :rx_addr_p0 -> 0x0A
      :rx_addr_p1 -> 0x0B
      :rx_addr_p2 -> 0x0C
      :rx_addr_p3 -> 0x0D
      :rx_addr_p4 -> 0x0E
      :rx_addr_p5 -> 0x0F
      :tx_addr -> 0x10
      :rx_pw_p0 -> 0x11
      :rx_pw_p1 -> 0x12
      :rx_pw_p2 -> 0x13
      :rx_pw_p3 -> 0x14
      :rx_pw_p4 -> 0x15
      :rx_pw_p5 -> 0x16
      :fifo_status -> 0x17
      :dynpd -> 0x1C
      :feature -> 0x1D
    end
  end

  defp command(cmd) do
    case cmd do
      :r_register -> 0x00
      :w_register -> 0x20
      :r_rx_pl_wid -> 0x60
      :r_rx_payload -> 0x61
      :w_tx_payload -> 0xA0
      :w_ack_payload -> 0xA8
      :w_tx_payload_no_ack -> 0xB0
      :flush_tx -> 0xE1
      :fluxh_rx -> 0xE2
      :reuse_tx_pl -> 0xE3
      :nop -> 0xFF
    end
  end

  defp to_bit(bit_mark) when is_binary(bit_mark), do: to_bit(String.to_atom(bit_mark))

  defp to_bit(bit_mark) do
    case bit_mark do
      :prim_rx -> 0
      :pwr_up -> 1
      :crco -> 2
      :en_crc -> 3
      :mask_max_rt -> 4
      :mask_tx_ds -> 5
      :mask_rx_dr -> 6
      :enaa_p0 -> 0
      :enaa_p1 -> 1
      :enaa_p2 -> 2
      :enaa_p3 -> 3
      :enaa_p4 -> 4
      :enaa_p5 -> 5
      :erx_p0 -> 0
      :erx_p1 -> 1
      :erx_p2 -> 2
      :erx_p3 -> 3
      :erx_p4 -> 4
      :erx_p5 -> 5
      :rf_dr_high -> 3
      :pll_lock -> 4
      :rf_dr_low -> 5
      :cont_wave -> 7
      :tx_full -> 0
      :max_rt -> 4
      :tx_ds -> 5
      :rx_dr -> 6
      :fifo_rx_empty -> 0
      :fifo_rx_full -> 1
      :fifo_tx_empty -> 4
      :fifo_tx_full -> 5
      :fifo_tx_reuse -> 6
      :dpl_p0 -> 0
      :dpl_p1 -> 1
      :dpl_p2 -> 2
      :dpl_p3 -> 3
      :dpl_p4 -> 4
      :dpl_p5 -> 5
      :en_dyn_ack -> 0
      :en_ack_pay -> 1
      :en_dpl -> 2
    end
  end

  defp set_bit(bit_mark) do
    1 <<< to_bit(bit_mark)
  end

  defp reset_bit(bit_mark) do
    set_bit(bit_mark)
    |> bnot()
  end

  defp config_to_string(spi) do
    config = read_register(spi, :config)

    details =
      [
        :mask_rx_dr,
        :mask_tx_ds,
        :mask_max_rt,
        :en_crc,
        :crco,
        :pwr_up,
        :prim_rx
      ]
      |> Enum.map(fn bit ->
        bit_str = Atom.to_string(bit) |> String.upcase()

        val = (config &&& 1 <<< to_bit(bit)) >>> to_bit(bit)
        "#{bit_str}=#{val}"
      end)
      |> Enum.join(" ")

    config_line = "CONFIG\t\t= #{as_hex(config)} (#{as_binary(config)})"

    "#{config_line} #{details}"
  end

  defp status_to_string(spi) do
    status = read_register(spi, :status)

    details =
      [
        :rx_dr,
        :tx_ds,
        :max_rt,
        :rx_p_no,
        :tx_full
      ]
      |> Enum.map(fn bit ->
        bit_str = Atom.to_string(bit) |> String.upcase()

        # Data pipe number for the payload available for reading from
        # rx_fifo takes 3 bits from 1 to 3
        if :rx_p_no == bit do
          val = status >>> 1 &&& 7
          "#{bit_str}=#{val} (#{as_binary(val, 3)})"
        else
          val = (status &&& 1 <<< to_bit(bit)) >>> to_bit(bit)
          "#{bit_str}=#{val}"
        end
      end)
      |> Enum.join(" ")

    status_line = "STATUS\t\t= #{as_hex(status)} (#{as_binary(status)})"

    "#{status_line} #{details}"
  end

  defp fifo_status_to_string(spi) do
    fifo_status = read_register(spi, :fifo_status)

    details =
      [
        :fifo_tx_reuse,
        :fifo_tx_full,
        :fifo_tx_empty,
        :fifo_rx_full,
        :fifo_rx_empty
      ]
      |> Enum.map(fn bit ->
        bit_str = Atom.to_string(bit) |> String.upcase()

        val = (fifo_status &&& 1 <<< to_bit(bit)) >>> to_bit(bit)
        "#{bit_str}=#{val}"
      end)
      |> Enum.join(" ")

    fifo_status_line = "FIFO_STATUS\t= #{as_hex(fifo_status)} (#{as_binary(fifo_status)})"

    "#{fifo_status_line} #{details}"
  end

  defp addresses_to_string(spi) do
    [
      :rx_addr_p0,
      :rx_addr_p1,
      :rx_addr_p2,
      :rx_addr_p3,
      :rx_addr_p4,
      :rx_addr_p5,
      :tx_addr
    ]
    |> Enum.map(fn reg -> address_to_string(reg, spi) end)
    |> Enum.join("\n")
  end

  defp address_to_string(register, spi) do
    val =
      cond do
        Enum.member?([:rx_addr_p0, :rx_addr_p1, :tx_addr], register) ->
          read_register(spi, register, 5)

        true ->
          reg_val = read_register(spi, register, 1)

          <<reg_val>>
      end

    hex =
      val
      |> :binary.bin_to_list()
      |> Enum.map(fn byte ->
        as_hex(byte, 2)
      end)
      |> Enum.join(" ")

    tab = if :tx_addr == register, do: "\t\t", else: "\t"

    "#{String.upcase(Atom.to_string(register))}#{tab}= #{inspect(val)} <<#{hex}>>"
  end

  defp payload_size_as_string(spi) do
    vals =
      [
        :rx_pw_p0,
        :rx_pw_p1,
        :rx_pw_p2,
        :rx_pw_p3,
        :rx_pw_p4,
        :rx_pw_p5
      ]
      |> Enum.map(fn reg -> as_hex(read_register(spi, reg)) end)
      |> Enum.join(" ")

    "RX_PW_P0-5\t= #{vals}"
  end

  defp register_to_string(register, spi) do
    val = read_register(spi, register)
    reg_str = register |> Atom.to_string() |> String.upcase()
    tabs = if 8 <= String.length(reg_str), do: "\t", else: "\t\t"

    "#{reg_str}#{tabs}= #{as_hex(val)}"
  end

  # Converts value to binary string representation of given length.
  defp as_binary(value, size \\ 8) do
    value
    |> Integer.to_string(2)
    |> String.pad_leading(size, "0")
  end

  # Converts value to hexadecimal string representation of given
  # length.
  defp as_hex(value, size \\ 2) do
    hex =
      value
      |> Integer.to_string(16)
      |> String.pad_leading(size, "0")

    "0x#{hex}"
  end
end
