# Nrf24

Elixir library for receiving and sending data through nRF24L01+
wireless transciver module. It was tested with pinout given below
which is not unique and if different pinout is used, with different CE
and CSN pins connections samples, should be adjusted accordingly.

## Pinout ##

![nRF24L01 Pinout](./assets/nRF24L01-pinout.png "nRF24L01 Pinout")

## Raspberry Pi Wiring

| nRF24L01+ | Rasbperry Pi 3B+ |
|----------:|-----------------:|
|       GND |          GND (6) |
|       VCC |         3.3V (1) |
|        CE |      GPIO17 (11) |
|       CSN |       GPIO8 (24) |
|       SCK |      GPIO11 (23) |
|      MOSI |      GPIO10 (19) |
|      MISO |       GPIO9 (21) |

![Rasberry PI 3B+ Pinout](./assets/raspberry-pi-3-b-plus-pinout.png "Raspberry PI 3 B+ Pinout")

## Arduino

### Wiring

Library was tested with the second module connected to Arduino Nano.

| nRF24L01+ | Arduino |
|----------:|--------:|
|       GND |     GND |
|       VCC |    +3V3 |
|        CE |      D9 |
|       CSN |     D10 |
|       SCK |     D13 |
|      MOSI |     D11 |
|      MISO |     D12 |

![Arduino Nano Pinout](./assets/arduino-nano-pinout.png "Arduino Nano Pinout")

### Library

[RF24 Arduino library](https://nrf24.github.io/RF24/ "RF24 Library")
example `GettingStarted` is used for communication on the Arduino side.

## Installation

Package can be installed by adding `nrf24` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:nrf24, "~> 1.0.0"}
  ]
end
```

## Usage

### Receiving data

```elixir
{:ok, nrf} =
  Nrf24.start_link(
    channel: 0x4C,
    ce_pin: 17,
    csn_pin: 0,
    crc_length: 2,
    speed: :medium,
    pipes: [[pipe_no: 0, address: "aaaaa", payload_size: 4, auto_ack: true]]
  )
GenServer.call(nrf, :start_listening)
GenServer.call(nrf, {:read_data, 4})
GenServer.call(nrf, :stop_listening)
```

### Sending data ###

```elixir
{:ok, nrf} =
  Nrf24.start_link(
    channel: 0x4C,
    ce_pin: 17,
    csn_pin: 0,
    crc_length: 2,
    speed: :medium,
  )
data = <<9273.69::float-little-size(32)>>
GenServer.cast(nrf, {:send, "bbbbb", data})
```
